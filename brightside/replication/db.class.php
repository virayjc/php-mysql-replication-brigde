<?php
    namespace Brightside\Replication;

    class db {
        var $debug = false;
        var $hostname = '';
        var $username = '';
        var $password = '';
        var $port = 3306;
        var $dbname = '';
        var $shortname = '';
        var $conn = null;

        function __construct($host, $user, $pass, $port = 3306) {
            $this->hostname = $host;
            $this->username = $user;
            $this->password = $pass;
            $this->port = $port;
        }

        function setShortName($shortname) {
            $this->shortname = $shortname;
        }

        function select_db($db) {
            $this->dbname = $db;
        }

        function connect() {
            if(is_null($this->conn)) {
                $this->conn = new mysqli($this->hostname, $this->username, $this->password,$this->dbname, $this->port);

                if($this->conn->connect_error) {
                    throw new Exception("Database Connection Error");
                }
            }
            if( $this->dbname ) {
                $this->conn->select_db($this->dbname);
            }
            $this->conn->set_charset('utf8mb4');
        }

        function close() {
            if(!is_null($this->conn)) {
                $this->conn->close();
                $this->conn = null;
            }
        }        

        function query($query) {
            print "Database: ".$this->dbname.', Query: '.$query."\n";
            $this->connect();
            $ret = $this->conn->query($query);
            return $ret;
        }


        function insert($table, $assoc, $onduplicatekey = array()) {
            foreach($assoc as $k => $v) {
                $keys[] = $k;

                if(is_null($v)) {
                    $vals[] = "NULL";
                } else {
                    $vals[] = "'".$this->real_escape_string($v)."'";
                }

            }


            if(is_string($onduplicatekey) && $onduplicatekey == "ignore") {
                $query = "INSERT IGNORE";
            } else { $query = "INSERT"; }

            $query .= " INTO {$table} ( ".join(',', $keys)." ) VALUES ( ".join(',', $vals)." )";

            if(is_array($onduplicatekey) && count($onduplicatekey) > 0) {
                if(!is_vector($onduplicatekey)) {
                    $update_list = array();
                    foreach($onduplicatekey as $k => $v) {
                        $update_list[] = "`".$this->real_escape_string($k)."` = '".$this->real_escape_string($v)."'";
                    }
                } else { $update_list = $onduplicatekey; }

                $query .= ' ON DUPLICATE KEY UPDATE '.join(', ', $update_list);
            }

            $this->query($query);

            return $this->conn->insert_id;
        }
        
        function insertignore($table, $assoc) {
            return $this->insert($table, $assoc, "ignore");
        }

        function update($table, $updates, $where) {
            $this->connect();

            $update_list = array();
            foreach($updates as $k => $v) {
                if(is_null($v)) {
                    $update_list[] = "`".$this->real_escape_string($k)."` = NULL";
                } else {
                    $update_list[] = "`".$this->real_escape_string($k)."` = '".$this->real_escape_string($v)."'";
                }
            }

            $where_list = array();
            foreach($where as $k => $v) {
                $where_list[] = "`".$this->real_escape_string($k)."` = '".$this->real_escape_string($v)."'";
            }

            if(count($where_list) > 0) {
                $query = "UPDATE ".$this->real_escape_string($table)." SET ".join(', ', $update_list)." WHERE ".join(' AND ', $where_list);
            } else {
                $query = "UPDATE ".$this->real_escape_string($table)." SET ".join(', ', $update_list);
            }

            $this->query($query);

            return true;
        }        

        function delete($table, $where) {
            $where_list = array(1);
            if( is_array( $where ) ) {
                foreach($where as $k => $v) {
                    $where_list[] = "`".$this->real_escape_string($k)."` = '".$this->real_escape_string($v)."'";
                }
            } else {
                $where_list [] = $where;
            }


            $query = "DELETE FROM ".$this->real_escape_string($table)." WHERE ".join(' AND ', $where_list);

            $this->query($query);

            return true;
        }        

        function fetchRow($query) {
            $res = $this->query($query);
            return $res->fetch_assoc();
        }
        
        function fetchAll($query, $index = '') {
            $res = $this->query($query);
            $ret = array();
            while($row = $res->fetch_assoc()) {
                if($index == '') {
                    $ret[] = $row;
                } else {
                    $ret[$row[$index]] = $row;
                }
            }
            return $ret;
        }

        function fetchOne($query) {
            $res = $this->query($query);
            list($ret) = $res->fetch_row();

            return $ret;
        }

        function debug($debug) {
            $this->debug = $debug;
        }

        function escape($value) {
            return "'".$this->real_escape_string($value)."'";
        }
        
        function real_escape_string($val) {
            $this->connect();

            return $this->conn->real_escape_string($val);
        }

        function expr($param, $fields) {
            foreach($fields as $k => $v) {
                $param = str_replace(":$k", $this->escape($v), $param);
            }
            return $param;
        }

        // just in case there's anything floating around in the wild ..

        function __get($name) {
            return $this->conn->$name;
        }

        function __call($method, $arguments) {
            return call_user_func_array(array($this->conn, $method), $arguments);
        }
    }


