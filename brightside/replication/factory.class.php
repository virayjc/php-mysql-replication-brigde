<?php
    namespace Brightside\Replication;

    class Factory {
        static $vars = array();

        static function set($key, $val) {
            Factory::$vars[$key] = $val;
        }

        static function get($key) {
            if(!isset(Factory::$vars[$key])) {
                return null;
            }
            return Factory::$vars[$key];
        }

        static function exists($key) {
            return isset(Factory::$vars[$key]);
        }

        static function setInterface($interface_id) {
            $db = Factory::get("db");
            Factory::set("interface", $db->fetchRow("SELECT * FROM interfaces WHERE id = ".intval($interface_id)));
        }
    }
