<?php
    namespace Brightside\Replication\Eventhandler;

    use MySQLReplication\Event\DTO\EventDTO;
    use MySQLReplication\Definitions\ConstEventsNames;
    use MySQLReplication\Event\EventSubscribers;

    class MysqlEventSubscriber extends EventSubscribers  {
        static $db = null;


        public function allEvents(EventDTO $event): void {
            $runQuery = false;
            if( in_array( $event->getType(), array(ConstEventsNames::UPDATE, ConstEventsNames::DELETE, ConstEventsNames::WRITE )) ) {
                $table = $event->getTableMap()->getTable();
                $database = $event->getTableMap()->getDatabase();
                MysqlEventSubscriber::$db->select_db( $database );
            }
            $status = false;
            switch ( $event->getType() ) {
                case ConstEventsNames::UPDATE:
                    foreach($event->getValues() as $values) {
                        $newValues = $values['after'];
                        $primaryKey = key( $newValues );
                        $primaryKeyValue = $newValues[$primaryKey];

                        $where = array($primaryKey => $primaryKeyValue);
                        $result = MysqlEventSubscriber::$db->fetchOne("SELECT * FROM {$table} WHERE {$primaryKey} = {$primaryKeyValue}");
                        if( $result == null ) {
                            $status = MysqlEventSubscriber::$db->insert($table, $newValues);
                        } else {
                            $status = MysqlEventSubscriber::$db->update($table, $newValues, $where);
                        }
                    }
                    $runQuery = true;
                    break;
                case ConstEventsNames::DELETE:
                    $deleteRows = array();
                    $rows = $event->getValues();
                    foreach ( $rows as $row ) {
                        $primaryKey = key( $row );
                        $deleteRows[] = $row[$primaryKey];
                    }
                    $where = "id IN (".join(',',$deleteRows).")";
                    $status = MysqlEventSubscriber::$db->delete($table, $where);
                    $runQuery = true;
                    break;
                case ConstEventsNames::WRITE:
                    $runQuery = true;

                    $insertValues = [];
                    $rows = $event->getValues();
                    foreach ( $rows as $values ) {
                        $vals=[];
                        $keys=[];
                        foreach($values as $k => $v) {
                            $keys[] = $k;

                            if(is_null($v)) {
                                $vals[] = "NULL";
                            } else {
                                $vals[] = "'".$v."'";
                            }
                        }
                        $insertValues[] = "( ".join(',', $vals)." )";
                    }
                    $status = MysqlEventSubscriber::$db->query("INSERT INTO `{$table}` ( ".join(',', $keys)." ) VALUES ".join(' , ', $insertValues)."");
                    break;
                case ConstEventsNames::QUERY:
                    $runQuery = true;

                    $status = MysqlEventSubscriber::$db->query($event->getQuery());
                    break;
            }

            if( $status || $status > 0 ){
                print "SQL Query Successfully executed to Slave.\n";
            } else if( $runQuery) {
                print "Failed to write. Reason: ".MysqlEventSubscriber::$db->error."\n";
            }
        }
    }