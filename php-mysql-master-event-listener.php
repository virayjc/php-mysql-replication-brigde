<?php 
	error_reporting(E_ALL);
	date_default_timezone_set('UTC');
	include __DIR__ . '/autoload.php';

    use Brightside\Replication\Factory;
    use Brightside\Replication\db;
    use Brightside\Replication\Eventhandler\MysqlEventSubscriber;

    use MySQLReplication\Config\ConfigBuilder;
	use MySQLReplication\Event\EventSubscribers;
	use MySQLReplication\MySQLReplicationFactory;

	$opts = getopt('s:');
	$validSystems = Factory::get('system-replications');
	if( !isset($opts['s']) || $opts['s'] == '' ) {
	    print "Missing system option: s. Exited.\n";
        exit;
    } else if( !in_array($opts['s'], $validSystems) ) {
        print "Invalid inputted system. Exited.\n";
        exit;
    }
	/**
	 * Your db configuration
	 * @see ConfigBuilder
	 * @link https://github.com/krowinski/php-mysql-replication/blob/master/README.md
	 */
	$master = Factory::get($opts['s'].'-master-db');
    $slave = Factory::get($opts['s'].'-slave-db');

    try {
        $binLogStream = new MySQLReplicationFactory(
            (new ConfigBuilder())
                ->withUser($master['username'])
                ->withHost($master['hostname'])
                ->withPassword($master['password'])
                ->withPort($master['port'])
                ->withSlaveId(100)
                ->withHeartbeatPeriod(2)
                ->build()
        );
    } catch ( Exception $e) {
        print $e->getMessage();
    }

    /**
	 * Register your events handler
	 * @see EventSubscribers
	 */

    MysqlEventSubscriber::$db = new db($slave['hostname'],$slave['username'],$slave['password'], $slave['port']);

    $mysqlEventSubscriber = new MysqlEventSubscriber();
	$binLogStream->registerSubscriber(
        $mysqlEventSubscriber
	);

	// start consuming events
	$binLogStream->run();

