<?php
    include __DIR__ . '/vendor/autoload.php';
    include __DIR__ . '/brightside/replication/db.class.php';
    include __DIR__ . '/brightside/replication/factory.class.php';
    include __DIR__ . '/brightside/replication/config.local.inc.php';
    include __DIR__ . '/brightside/replication/eventhandler/MysqlEventSubscriber.php';